package com.example.moosik


import android.content.ContentUris
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.text.method.ScrollingMovementMethod
import android.widget.ImageButton
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SwitchCompat
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import okhttp3.*
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


class MainActivity : AppCompatActivity() {


    private val requestCode = 123
    private var client = OkHttpClient()
    private lateinit var playPause: ImageButton
    private lateinit var next: ImageButton
    private lateinit var previous: ImageButton
    private var audioList: ArrayList<Audio> = arrayListOf()
    private var mMediaPlayer: MediaPlayer? = null
    private lateinit var currentSong: ListIterator<Audio>
    private lateinit var albumView: TextView
    private lateinit var timeElapsedView: TextView
    private lateinit var totalTimeView: TextView
    private lateinit var songAndArtistView: TextView
    private lateinit var lyricsView : TextView
    private lateinit var seekBar: SeekBar
    private lateinit var btnSwView: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        periodicityHandler()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ActivityCompat.requestPermissions(this, arrayOf(
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        ),
            requestCode
        )
        lyricsView  = findViewById(R.id.lyrics)
        lyricsView.movementMethod = ScrollingMovementMethod()
        albumView = findViewById(R.id.album)
        seekBar = findViewById(R.id.seekBar)
        timeElapsedView = findViewById(R.id.timeElapsed)
        totalTimeView = findViewById(R.id.totalTime)
        songAndArtistView = findViewById(R.id.songAndArtist)

        //fetch list of files in the external storage of the phone
        audioList = fetchAudio()
        currentSong = audioList.listIterator()

        // binding functions to buttons

        btnSwView = findViewById(R.id.buttonSwView)
        btnSwView.setOnClickListener {
            val myIntent = Intent(this, ListViewActivity::class.java)
            startActivity(myIntent)
        }

        playPause = findViewById(R.id.playSong)
        playPause.setOnClickListener {
            pauseSong()
        }

        next = findViewById(R.id.nextSong)
        next.setOnClickListener {
            nextSong()
        }

        previous = findViewById(R.id.previousSong)
        previous.setOnClickListener {
            previousSong()
        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, currentPosition: Int, userAction: Boolean) {
                if(userAction) {
                    seekTo(currentPosition)
                    updateElapsedTime()
                }
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) { }
            override fun onStopTrackingTouch(seekBar: SeekBar) { }
        })

        // dark mode
        val toggle: SwitchCompat = findViewById(R.id.toggleDarkMode)
        toggle.setOnCheckedChangeListener { _, isChecked ->
            val message = if (isChecked) "Dark Mode ON" else "Dark Mode OFF"
            val mode =
                if (isChecked) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
            AppCompatDelegate.setDefaultNightMode(mode)
            delegate.applyDayNight()
            Toast.makeText(
                this@MainActivity, message,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun fetchAudio(): ArrayList<Audio> {
        val collection = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.DISPLAY_NAME,
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.ALBUM,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.DURATION,
        )
        val selection = "${MediaStore.Audio.Media.IS_MUSIC} != 0"
        applicationContext.contentResolver.query(
            collection,
            projection,
            selection,
            null,
            null
        )?.use { cursor ->

            // Cache column indices.

            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
            val nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
            val titleColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
            val albumColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM)
            val artistColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST)
            val durationColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)

            while (cursor.moveToNext()) {

                // Use an ID column from the projection to get
                // a URI representing the media item itself.

                val id = cursor.getLong(idColumn)
                val displayName = cursor.getString(nameColumn)
                val artist = cursor.getString(artistColumn)
                val title = cursor.getString(titleColumn)
                val album = cursor.getString(albumColumn)
                val duration = cursor.getInt(durationColumn)
                val contentUri: Uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id)
                audioList.add(
                    Audio(
                        uri = contentUri,
                        name = displayName,
                        title = title,
                        artist = artist,
                        album = album,
                        duration = duration
                    )
                )
            }
        }
        return audioList
    }

    private fun playContentUri(audio: Audio) {
        val uri = audio.uri
        try {
            mMediaPlayer = MediaPlayer().apply {
                setDataSource(application, uri)
                setAudioAttributes(
                    AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build()
                )
                setOnCompletionListener {
                    nextSong()
                }
                prepare()
            }
            updateInfoOnSongChange(audio)
            mMediaPlayer!!.start()
        } catch (e: IOException) {
            mMediaPlayer = null
            mMediaPlayer?.release()
        }
    }

    // Play/Pause/Resume playback
    private fun pauseSong() {
        if(mMediaPlayer == null){
            playContentUri(currentSong.next())
            playPause.setImageResource(R.drawable.ic_baseline_pause_circle_outline)
        } else {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.pause()
                playPause.setImageResource(R.drawable.ic_baseline_play_circle_outline)
            } else {
                mMediaPlayer!!.start()
                playPause.setImageResource(R.drawable.ic_baseline_pause_circle_outline)
            }

        }
    }
    // stop song playback on skip
    private fun stopSong() {
        if(mMediaPlayer != null){
            mMediaPlayer!!.stop()
        }
    }

    private fun nextSong() {
        if (currentSong.hasNext()) {
            stopSong()
            playContentUri(currentSong.next())
            Toast.makeText(
                this@MainActivity, "Playing next Song",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun previousSong() {
        if (currentSong.hasPrevious()) {
            stopSong()
            playContentUri(currentSong.previous())
            Toast.makeText(
                this@MainActivity, "Playing previous Song",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun updateInfoOnSongChange(audio: Audio) {
        //run("https://api.lyrics.ovh/v1/${audio.artist.replace(" ","%20")}/${audio.title.replace(" ", "%20")}")
        albumView.text = audio.album
        "00:00".also { timeElapsedView.text = it }
        totalTimeView.text = getTimeFromDuration(audio.duration)
        "${audio.title} - ${audio.artist}".also { songAndArtistView.text = it }
        seekBar.max = audio.duration / 1000
    }

    private fun stopMedia(){
        if (mMediaPlayer != null) {
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
    }

    // Closes the MediaPlayer when the app is closed
    override fun onStop() {
        super.onStop()
        stopMedia()
    }

    private fun run(url: String) {

        val request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}
            override fun onResponse(call: Call, response: Response) {
                GsonConverterFactory.create(
                )
                if(response.isSuccessful){
                    val body = Gson().fromJson(response.body()!!.string(),ApiResponse::class.java)
                    lyricsView.text = body.lyrics
                } else
                    "Could not fetch lyrics online".also { lyricsView.text = it }
            }
        })
    }

    // Seeks to given position in media when called
    private fun seekTo(value:Int) {
        if (mMediaPlayer != null && mMediaPlayer!!.isPlaying) {
            mMediaPlayer!!.seekTo(value * 1000)
        }
    }

    // Formats a duration into legible timestamp
    private fun getTimeFromDuration(duration : Int):String{
        return "${"%02d".format(duration/60000)}:${"%02d".format((duration/1000)%60)}"
    }

    // Updates the timestamp using position from inside the current media
    private fun updateElapsedTime(){
        if (mMediaPlayer != null && mMediaPlayer!!.isPlaying) {
            timeElapsedView.text = getTimeFromDuration(mMediaPlayer!!.currentPosition)
            seekBar.progress = mMediaPlayer!!.currentPosition /1000
        }
    }

    // Creates a call each second to update elements such as seekbar and elapsed time
    private fun periodicityHandler() {
        var runnable: Runnable? = null
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed(Runnable {
            handler.postDelayed(runnable!!, 1000)
            updateElapsedTime()
        }.also { runnable = it }, 1000)
    }

    inner class ApiResponse(val lyrics : String)
}