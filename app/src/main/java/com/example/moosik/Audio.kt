package com.example.moosik
import android.net.Uri

data class Audio(val uri: Uri,
                 val name: String,
                 val title: String,
                 val album: String,
                 val artist: String,
                 val duration: Int )